package com.example.demo;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import io.netty.handler.codec.MessageToMessageEncoder;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Scope("prototype")
public class MyEncoder  extends MessageToByteEncoder<String> {

    @Override
//    protected void encode(ChannelHandlerContext ctx, String msg, List<Object> out) throws Exception {
//        out.add(msg.toString());
//    }

    protected void encode(ChannelHandlerContext ctx, String msg, ByteBuf out) {

        //out.writeBytes(msg.getBytes()) ;
        out.writeBytes(msg.toUpperCase().getBytes());
    }
}