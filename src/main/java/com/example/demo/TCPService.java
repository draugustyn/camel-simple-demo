package com.example.demo;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.netty.NettyConstants;
import org.springframework.stereotype.Component;

@Component
public class TCPService extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        from("netty:tcp://0.0.0.0:5155?sync=true&encoders=#myEncoder")
                .process(new Processor() {
                    public void process(Exchange exchange) throws Exception {
                        String body = exchange.getIn().getBody(String.class);
                        exchange.getOut().setBody("Received " + body);
                        log.info(body);
                    }
                });

        from("netty:tcp://0.0.0.0:5156?sync=false")
                .process(new Processor() {
                    public void process(Exchange exchange) throws Exception {
                        String body = exchange.getIn().getBody(String.class);
                        // some condition which determines if we should close
                        log.info(body);
                        if( body.substring(0,1).equals("c")){
                            exchange.getOut().setHeader(NettyConstants.NETTY_CLOSE_CHANNEL_WHEN_COMPLETE, true);
                        }
                    }
                });

    }
}


// https://www.mastertheboss.com/jboss-frameworks/camel/using-camel-netty-components-to-manage-socket-routes/
// .to("stream:out");
// http://shengwangi.blogspot.com/2015/01/camel-netty-hello-world-simple-example.html
// .to("log:?level=INFO&showBody=true");

